`timescale 1ns/1ps
`include "const.vh"

module doubleround (
    input wire          CLK,
    input wire          RST,
    input wire  [63 :0] IDX,
    output wire [511:0] Y
);

    reg  [31 : 0] matr   [0 :  19][0 : 15];
    wire [31 : 0] matr_c [0 :  19][0 : 15];
    reg  [31 : 0] mem    [0 :  19][0 : 15];
 
    reg  [31 : 0] rez    [0 : 15];
    wire [31 : 0] rez_c  [0 : 15];
 
    wire [31 : 0] int_x  [0 : 15];
    wire [255: 0] key;
    wire [63 : 0] nonce;

    integer j;


    assign key   = `KEY;
    assign nonce = `NONCE;

    assign int_x[ 0] = `CONST_0;
    assign int_x[ 1] =  key  [31  :  0];
    assign int_x[ 2] =  key  [63  : 32];
    assign int_x[ 3] =  key  [95  : 64];
    assign int_x[ 4] =  key  [127 : 96];
    assign int_x[ 5] = `CONST_1;
    assign int_x[ 6] =  nonce[31  :  0];
    assign int_x[ 7] =  nonce[63  : 32];
    assign int_x[ 8] =  IDX  [31  :  0];
    assign int_x[ 9] =  IDX  [63  : 32];
    assign int_x[10] = `CONST_2;
    assign int_x[11] =  key  [159 :128];
    assign int_x[12] =  key  [191 :160];
    assign int_x[13] =  key  [223 :192];
    assign int_x[14] =  key  [255 :224];
    assign int_x[15] = `CONST_3;


    assign matr_c[0][ 4] = int_x[ 4] ^ rol_7(int_x[12] + int_x[ 0]);
    assign matr_c[0][ 9] = int_x[ 9] ^ rol_7(int_x[ 1] + int_x[ 5]);
    assign matr_c[0][14] = int_x[14] ^ rol_7(int_x[ 6] + int_x[10]);
    assign matr_c[0][ 3] = int_x[ 3] ^ rol_7(int_x[11] + int_x[15]);

    assign matr_c[0][ 8] = int_x[ 8] ^ rol_9(int_x[ 0] + matr_c[0][ 4]);
    assign matr_c[0][13] = int_x[13] ^ rol_9(int_x[ 5] + matr_c[0][ 9]);
    assign matr_c[0][ 2] = int_x[ 2] ^ rol_9(int_x[10] + matr_c[0][14]);
    assign matr_c[0][ 7] = int_x[ 7] ^ rol_9(int_x[15] + matr_c[0][ 3]);

    assign matr_c[0][12] = int_x[12] ^ rol_13(matr_c[0][ 4] + matr_c[0][ 8]);
    assign matr_c[0][ 1] = int_x[ 1] ^ rol_13(matr_c[0][ 9] + matr_c[0][13]);
    assign matr_c[0][ 6] = int_x[ 6] ^ rol_13(matr_c[0][14] + matr_c[0][ 2]);
    assign matr_c[0][11] = int_x[11] ^ rol_13(matr_c[0][ 3] + matr_c[0][ 7]);

    assign matr_c[0][ 0] = int_x[ 0] ^ rol_18(matr_c[0][ 8] + matr_c[0][12]);
    assign matr_c[0][ 5] = int_x[ 5] ^ rol_18(matr_c[0][13] + matr_c[0][ 1]);
    assign matr_c[0][10] = int_x[10] ^ rol_18(matr_c[0][ 2] + matr_c[0][ 6]);
    assign matr_c[0][15] = int_x[15] ^ rol_18(matr_c[0][ 7] + matr_c[0][11]);


    assign matr_c[1][ 1] = matr[0][ 1] ^ rol_7(matr[0][ 3] + matr[0][ 0]);
    assign matr_c[1][ 6] = matr[0][ 6] ^ rol_7(matr[0][ 4] + matr[0][ 5]);
    assign matr_c[1][11] = matr[0][11] ^ rol_7(matr[0][ 9] + matr[0][10]);
    assign matr_c[1][12] = matr[0][12] ^ rol_7(matr[0][14] + matr[0][15]);

    assign matr_c[1][ 2] = matr[0][ 2] ^ rol_9(matr[0][ 0] + matr_c[1][ 1]);
    assign matr_c[1][ 7] = matr[0][ 7] ^ rol_9(matr[0][ 5] + matr_c[1][ 6]);
    assign matr_c[1][ 8] = matr[0][ 8] ^ rol_9(matr[0][10] + matr_c[1][11]);
    assign matr_c[1][13] = matr[0][13] ^ rol_9(matr[0][15] + matr_c[1][12]);

    assign matr_c[1][ 3] = matr[0][ 3] ^ rol_13(matr_c[1][ 1] + matr_c[1][ 2]);
    assign matr_c[1][ 4] = matr[0][ 4] ^ rol_13(matr_c[1][ 6] + matr_c[1][ 7]);
    assign matr_c[1][ 9] = matr[0][ 9] ^ rol_13(matr_c[1][11] + matr_c[1][ 8]);
    assign matr_c[1][14] = matr[0][14] ^ rol_13(matr_c[1][12] + matr_c[1][13]);

    assign matr_c[1][ 0] = matr[0][ 0] ^ rol_18(matr_c[1][ 2] + matr_c[1][ 3]);
    assign matr_c[1][ 5] = matr[0][ 5] ^ rol_18(matr_c[1][ 7] + matr_c[1][ 4]);
    assign matr_c[1][10] = matr[0][10] ^ rol_18(matr_c[1][ 8] + matr_c[1][ 9]);
    assign matr_c[1][15] = matr[0][15] ^ rol_18(matr_c[1][13] + matr_c[1][14]);



    always @(posedge CLK) begin
        if(RST) begin
            for (j = 0; j < 16; j = j + 1) begin
                matr[0][j] <= 'd0;
                matr[1][j] <= 'd0;
                mem [0][j] <= 'd0;
                mem [1][j] <= 'd0;
            end
        end
        else begin
            for (j = 0; j < 16; j = j + 1) begin
                mem [0][j] <= int_x[j];
                mem [1][j] <= int_x[j];
                matr[0][j] <= matr_c[0][j];
                matr[1][j] <= matr_c[1][j];
            end
        end
    end


    genvar i;
    generate
        for (i = 2; i < 20; i = i + 2) begin : pipeline

            assign matr_c[i][ 4] = matr[i - 1][ 4] ^ rol_7(matr[i - 1][12] + matr[i - 1][ 0]);
            assign matr_c[i][ 9] = matr[i - 1][ 9] ^ rol_7(matr[i - 1][ 1] + matr[i - 1][ 5]);
            assign matr_c[i][14] = matr[i - 1][14] ^ rol_7(matr[i - 1][ 6] + matr[i - 1][10]);
            assign matr_c[i][ 3] = matr[i - 1][ 3] ^ rol_7(matr[i - 1][11] + matr[i - 1][15]);

            assign matr_c[i][ 8] = matr[i - 1][ 8] ^ rol_9(matr[i - 1][ 0] + matr_c[i][ 4]);
            assign matr_c[i][13] = matr[i - 1][13] ^ rol_9(matr[i - 1][ 5] + matr_c[i][ 9]);
            assign matr_c[i][ 2] = matr[i - 1][ 2] ^ rol_9(matr[i - 1][10] + matr_c[i][14]);
            assign matr_c[i][ 7] = matr[i - 1][ 7] ^ rol_9(matr[i - 1][15] + matr_c[i][ 3]);

            assign matr_c[i][12] = matr[i - 1][12] ^ rol_13(matr_c[i][ 4] + matr_c[i][ 8]);
            assign matr_c[i][ 1] = matr[i - 1][ 1] ^ rol_13(matr_c[i][ 9] + matr_c[i][13]);
            assign matr_c[i][ 6] = matr[i - 1][ 6] ^ rol_13(matr_c[i][14] + matr_c[i][ 2]);
            assign matr_c[i][11] = matr[i - 1][11] ^ rol_13(matr_c[i][ 3] + matr_c[i][ 7]);

            assign matr_c[i][ 0] = matr[i - 1][ 0] ^ rol_18(matr_c[i][ 8] + matr_c[i][12]);
            assign matr_c[i][ 5] = matr[i - 1][ 5] ^ rol_18(matr_c[i][13] + matr_c[i][ 1]);
            assign matr_c[i][10] = matr[i - 1][10] ^ rol_18(matr_c[i][ 2] + matr_c[i][ 6]);
            assign matr_c[i][15] = matr[i - 1][15] ^ rol_18(matr_c[i][ 7] + matr_c[i][11]);
            
            //Colomn convert
            assign matr_c[i + 1][ 1] = matr[i][ 1] ^ rol_7(matr[i][ 3] + matr[i][ 0]);
            assign matr_c[i + 1][ 6] = matr[i][ 6] ^ rol_7(matr[i][ 4] + matr[i][ 5]);
            assign matr_c[i + 1][11] = matr[i][11] ^ rol_7(matr[i][ 9] + matr[i][10]);
            assign matr_c[i + 1][12] = matr[i][12] ^ rol_7(matr[i][14] + matr[i][15]);

            assign matr_c[i + 1][ 2] = matr[i][ 2] ^ rol_9(matr[i][ 0] + matr_c[i + 1][ 1]);
            assign matr_c[i + 1][ 7] = matr[i][ 7] ^ rol_9(matr[i][ 5] + matr_c[i + 1][ 6]);
            assign matr_c[i + 1][ 8] = matr[i][ 8] ^ rol_9(matr[i][10] + matr_c[i + 1][11]);
            assign matr_c[i + 1][13] = matr[i][13] ^ rol_9(matr[i][15] + matr_c[i + 1][12]);

            assign matr_c[i + 1][ 3] = matr[i][ 3] ^ rol_13(matr_c[i + 1][ 1] + matr_c[i + 1][ 2]);
            assign matr_c[i + 1][ 4] = matr[i][ 4] ^ rol_13(matr_c[i + 1][ 6] + matr_c[i + 1][ 7]);
            assign matr_c[i + 1][ 9] = matr[i][ 9] ^ rol_13(matr_c[i + 1][11] + matr_c[i + 1][ 8]);
            assign matr_c[i + 1][14] = matr[i][14] ^ rol_13(matr_c[i + 1][12] + matr_c[i + 1][13]);

            assign matr_c[i + 1][ 0] = matr[i][ 0] ^ rol_18(matr_c[i + 1][ 2] + matr_c[i + 1][ 3]);
            assign matr_c[i + 1][ 5] = matr[i][ 5] ^ rol_18(matr_c[i + 1][ 7] + matr_c[i + 1][ 4]);
            assign matr_c[i + 1][10] = matr[i][10] ^ rol_18(matr_c[i + 1][ 8] + matr_c[i + 1][ 9]);
            assign matr_c[i + 1][15] = matr[i][15] ^ rol_18(matr_c[i + 1][13] + matr_c[i + 1][14]);


            always @(posedge CLK) begin
                if(RST) begin
                    for (j = 0; j < 16; j = j + 1) begin
                        mem [i    ][j] <= 'd0;
                        mem [i + 1][j] <= 'd0;
                        matr[i    ][j] <= 'd0;
                        matr[i + 1][j] <= 'd0;
                    end
                end
                else begin
                    for (j = 0; j < 16; j = j + 1) begin
                        mem [i    ][j] <= mem[i - 1][j];
                        mem [i + 1][j] <= mem[i - 1][j];
                        matr[i    ][j] <= matr_c[i    ][j];
                        matr[i + 1][j] <= matr_c[i + 1][j];
                    end
                end
            end
        end
    endgenerate


    genvar k;
    generate
        for (k = 0; k < 16; k = k + 1) begin : rezult_generate
            assign rez_c[k] = mem[19][k] + matr[19][k];
            
            always @(posedge CLK) begin
                if(RST) begin
                    rez[k] <= 0;
                end
                else begin
                    rez[k] <= rez_c[k];
                end
            end

            assign Y[k * 32 + 31 : k * 32] = rez[k];
        end
    endgenerate


    function [31 : 0] rol_7;
    input [31: 0] x;
    begin
        rol_7 = {x[24 : 0], x[31 : 25]};
    end
    endfunction
    function [31 : 0] rol_9;
    input [31: 0] x;
    begin
        rol_9 = {x[22 : 0], x[31 : 23]};
    end
    endfunction
    function [31 : 0] rol_13;
    input [31: 0] x;
    begin
        rol_13 = {x[18 : 0], x[31 : 19]};
    end
    endfunction
    function [31 : 0] rol_18;
    input [31: 0] x;
    begin
        rol_18 = {x[13 : 0], x[31 : 14]};
    end
    endfunction
endmodule
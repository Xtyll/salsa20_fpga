`timescale 1ns/100ps

module Salsa20_tb ();

    reg  CLK;
    reg  RST;
    reg  RX;
    wire TX;


    wire [7 : 0] mem [0 : 64];

    integer j;


    Salsa20 Salsa20_uut(
        .CLK  (CLK),
        .RST_N(RST),
        .RX   (RX),
        .TX   (TX)
    );


    always #5
        CLK = ~CLK;


    initial begin
        #3000000
        $finish;
    end


    initial begin
        CLK = 1'b0;
        RST = 1'b0;
        RX  = 1'b1;
        #100
        RST = 1'b1;
        for (j = 0; j < 10; j = j + 1) begin
            send_data(mem[j]);
        end
    end



    task send_data;
        input reg[7:0] data;
        integer i;

        begin
            #270
            RX = 1'b0;
            for(i = 0; i < 8; i = i + 1) begin
                #270
                RX = data[i];
            end
            #270
            RX = 1'b1;
        end
    endtask : send_data


    assign mem[ 0] = 8'h06;
    assign mem[ 1] = 8'h48;
    assign mem[ 2] = 8'h65;
    assign mem[ 3] = 8'h6C;
    assign mem[ 4] = 8'h6C;
    assign mem[ 5] = 8'h6F;
    assign mem[ 6] = 8'h20;

    assign mem[ 7] = 8'h02;
    assign mem[ 8] = 8'h6F;
    assign mem[ 9] = 8'h57;


    assign mem[10] = 8'h06;
    assign mem[11] = 8'h20;
    assign mem[12] = 8'h6F;
    assign mem[13] = 8'h6C;
    assign mem[14] = 8'h6C;
    assign mem[15] = 8'h65;
    assign mem[16] = 8'h48;
endmodule
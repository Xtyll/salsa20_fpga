`timescale 1ns/1ps
`include "const.vh"

module Salsa20 (
    input  wire CLK,
    input  wire RST_N,
    input  wire RX,

    output wire TX,

    output reg  [3 : 0] main_state,
    output reg          uart_rcv,
    output wire [7 : 0] len
);

    wire         slow_clk;
    wire         rst;

    wire [511:0] hash;
    reg  [63 :0] idx;

    reg  [7 : 0] in_pkg_len;
    reg  [7 : 0] pkg_len;
    reg  [7 : 0] pkg_len_t;

    wire [7 : 0] rez_msg[0 : 63];
    reg  [7 : 0] in_msg [0 : 63];
    reg  [7 : 0] rez_reg[0 : 63];
    reg  [7 : 0] to_send[0 : 63];
    
    wire         is_transmitting;
    wire [7 : 0] rxd_data;
    wire         rxd_vld;
    reg  [7 : 0] txd_data;
    reg          txd_vld;
    
    reg          end_rcv;
    reg          start_send;
    reg          end_send;


    assign len   = in_pkg_len;
    assign rst   = ~RST_N;


    genvar i;
    generate
        for (i = 0; i < 64; i = i + 1) begin : rezult_gen
            assign rez_msg[i] = in_msg[i] ^ hash[i * 8 +  7 : i * 8];

            always @(posedge slow_clk or posedge rst) begin
                if(rst) begin
                    rez_reg[i] = 8'h00;
                end
                else begin
                    if(end_rcv) begin
                        rez_reg[i] = rez_msg[i];
                    end
                end
            end

            always @(posedge slow_clk or posedge rst) begin
                if(rst) begin
                    to_send[i] = 8'h00;
                end
                else begin
                    if(start_send == 1) begin
                        to_send[i] = rez_reg[i];
                    end
                end
            end
        end
    endgenerate


    always @(posedge slow_clk or posedge rst) begin : cpy
        reg latch;
        if(rst) begin
            pkg_len_t  = 8'h00;
            pkg_len    = 8'h00;
        end
        else begin
            if(end_rcv) begin
                pkg_len_t  = in_pkg_len;
                latch      = 1'b1;
            end
            if(latch == 1 && end_send == 1) begin
                latch      = 1'b0;
                pkg_len    = pkg_len_t;
                start_send = 1'b1;
            end
            else begin
                start_send = 1'b0;
            end
        end
    end


    always @(posedge slow_clk or posedge rst) begin
        if(rst) begin
            uart_rcv       = 1'b1;
        end
        else begin
            if(rxd_vld) begin
                uart_rcv   = ~uart_rcv;
            end
        end
    end


    always @(posedge slow_clk or posedge rst) begin : in_fsm
        reg [3 : 0] in_state;
        reg [7 : 0] addr;
        if(rst) begin
            in_state <= 4'd0;
            idx       = 64'h0000_0000_0000_0000;
        end
        else begin
            case (in_state)
                0 : begin
                    end_rcv = 1'b0;
                    if(rxd_vld) begin
                        in_state <= 4'd1;
                    end
                end
                
                1 : begin
                    if(rxd_data == 8'hFF) begin
                        in_state   <= 4'd0;
                        idx      = 64'h0000_0000_0000_0000;
                    end
                    else begin
                        in_pkg_len = rxd_data;
                        addr       = 8'd0;
                        idx        = idx + 64'h0000_0000_0000_0001;
                        in_state     <= 4'd2;
                    end
                end

                2 : begin
                    if(rxd_vld) begin
                        in_state <= 4'd3;
                    end 
                end

                3 : begin
                    in_msg[addr] = rxd_data;
                    if(addr == in_pkg_len - 1) begin
                        in_state <= 4'd5;
                        end_rcv = 1'b1;
                    end
                    else begin
                        in_state <= 4'd4;
                    end
                end

                4 : begin
                    addr      = addr + 8'd1;
                    in_state <= 4'd2;
                end

                5 : begin
                    in_state  <= 4'd0;
                end
                default : in_state <= 4'd0;
            endcase
            main_state = in_state;
        end
    end


    always @(posedge slow_clk or posedge rst) begin : send_fsm
        reg [4 : 0] send_state;
        reg [7 : 0] send_addr;
        if(rst) begin
            send_state    <= 4'd0;
            send_addr = 8'h00;
            end_send  = 1'b1;
        end
        else begin
            case (send_state)
                0 : begin
                    if(start_send == 1) begin
                        end_send    = 1'b0;
                        send_state <= 4'b1;
                        send_addr   = 8'h00;
                    end
                end

                1 : begin
                    txd_data    = to_send[send_addr];
                    send_state <= 4'd2;
                end

                2 : begin
                    if(!is_transmitting) begin
                        txd_vld     = 1'b1;
                        send_addr   = send_addr + 8'h01;
                        send_state <= 4'd3;
                    end
                end

                3 : begin
                    txd_vld = 1'b0;
                    if(send_addr == pkg_len) begin
                        send_state <= 4'd4;
                    end
                    else begin
                        send_state <= 4'd1;
                    end
                end

                4 : begin
                    end_send    = 1'b1;
                    send_state <= 4'd0;
                end
                default : send_state <= 4'd0;
            endcase
        end
    end


    doubleround doubleround_inst(
        .CLK(slow_clk),
        .RST(rst),
        .IDX(idx),
        .Y  (hash)
    );


    uart_core uart_core_inst(
        .clk            (slow_clk),
        .reset_n        (RST_N),
        .rxd            (RX),
        .txd            (TX),
        .rxd_syn        (rxd_vld),
        .rxd_data       (rxd_data),
        .rxd_ack        (),
        .txd_syn        (txd_vld),
        .txd_data       (txd_data),
        .txd_ack        (),
        .is_transmitting(is_transmitting)
    );


    PLLE2_BASE #(
        .BANDWIDTH("OPTIMIZED"),  // OPTIMIZED, HIGH, LOW
        .CLKFBOUT_MULT(10),        // Multiply value for all CLKOUT, (2-64)
        .CLKFBOUT_PHASE(0.0),     // Phase offset in degrees of CLKFB, (-360.000-360.000).
        .CLKIN1_PERIOD(10.0),      // Input clock period in ns to ps resolution (i.e. 33.333 is 30 MHz).
        // CLKOUT0_DIVIDE - CLKOUT5_DIVIDE: Divide amount for each CLKOUT (1-128)
        .CLKOUT0_DIVIDE(20),
        .CLKOUT1_DIVIDE(1),
        .CLKOUT2_DIVIDE(1),
        .CLKOUT3_DIVIDE(1),
        .CLKOUT4_DIVIDE(1),
        .CLKOUT5_DIVIDE(1),
        // CLKOUT0_DUTY_CYCLE - CLKOUT5_DUTY_CYCLE: Duty cycle for each CLKOUT (0.001-0.999).
        .CLKOUT0_DUTY_CYCLE(0.5),
        .CLKOUT1_DUTY_CYCLE(0.5),
        .CLKOUT2_DUTY_CYCLE(0.5),
        .CLKOUT3_DUTY_CYCLE(0.5),
        .CLKOUT4_DUTY_CYCLE(0.5),
        .CLKOUT5_DUTY_CYCLE(0.5),
        // CLKOUT0_PHASE - CLKOUT5_PHASE: Phase offset for each CLKOUT (-360.000-360.000).
        .CLKOUT0_PHASE(0.0),
        .CLKOUT1_PHASE(0.0),
        .CLKOUT2_PHASE(0.0),
        .CLKOUT3_PHASE(0.0),
        .CLKOUT4_PHASE(0.0),
        .CLKOUT5_PHASE(0.0),
        .DIVCLK_DIVIDE(1),        // Master division value, (1-56)
        .REF_JITTER1(0.0),        // Reference input jitter in UI, (0.000-0.999).
        .STARTUP_WAIT("FALSE")    // Delay DONE until PLL Locks, ("TRUE"/"FALSE")
    )
    PLLE2_BASE_inst (
        // Clock Outputs: 1-bit (each) output: User configurable clock outputs
        .CLKOUT0(slow_clk),   // 1-bit output: CLKOUT0
        .CLKOUT1(),   // 1-bit output: CLKOUT1
        .CLKOUT2(),   // 1-bit output: CLKOUT2
        .CLKOUT3(),   // 1-bit output: CLKOUT3
        .CLKOUT4(),   // 1-bit output: CLKOUT4
        .CLKOUT5(),   // 1-bit output: CLKOUT5
        // Feedback Clocks: 1-bit (each) output: Clock feedback ports
        .CLKFBOUT(CLKFBOUT), // 1-bit output: Feedback clock
        .LOCKED(),     // 1-bit output: LOCK
        .CLKIN1(CLK),     // 1-bit input: Input clock
        // Control Ports: 1-bit (each) input: PLL control ports
        .PWRDWN(),     // 1-bit input: Power-down
        .RST(rst),           // 1-bit input: Reset
        // Feedback Clocks: 1-bit (each) input: Clock feedback ports
        .CLKFBIN(CLKFBOUT)    // 1-bit input: Feedback clock
    );
endmodule